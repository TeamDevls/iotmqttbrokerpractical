#include "subscriptionwidget.h"

#include <QGridLayout>

SubscriptionWidget::SubscriptionWidget(TopicStruct& topic, QWidget* parent)
    : BaseWidget(parent)
{
    setColorScheme(ColorScheme::SECONDARY);
    setRadius(8);
    topic_ = topic;
    auto main_layout = new QVBoxLayout;
    main_layout->setSpacing(0);
    setLayout(main_layout);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    main_layout->setContentsMargins(0, 0, 0, 0);

    auto header = new AnimatedWidget;
    header->setColorScheme(ColorScheme::SECONDARY);
    header->setRadius(8);
    auto header_layout = new QHBoxLayout;
    header->setLayout(header_layout);
    header->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    connect(header,
        &AnimatedWidget::clicked,
        this,
        [=]() {
            if (auto height = content_area->height(); height == 0) {
                header->setColorScheme(ColorScheme::PRIMARY);
                setColorScheme(ColorScheme::PRIMARY);
                animation->setStartValue(height);
                animation->setEndValue(content_area->sizeHint().height());
                animation->start();
            } else {
                header->setColorScheme(ColorScheme::SECONDARY);
                setColorScheme(ColorScheme::SECONDARY);
                content_area->hide();
                animation->setStartValue(height);
                animation->setEndValue(0);
                animation->start();
            }
        });
    topic_alias_value_ = new TextWidget(topic.alias, 10, true, ColorScheme::TRANSPARENT);
    subscription_btn_ = new AnimatedButton(tr("Unsubscribe from topic"), ColorScheme::PRIMARY, 8);
    subscription_btn_->setMinimumHeight(subscription_btn_->sizeHint().height() * 1.5);
    header_layout->addWidget(topic_alias_value_, 4);
    header_layout->addWidget(subscription_btn_, 1);

    topic_label_value_ = new TextWidget(topic_.subscription->topic().filter());
    topic_status_value_ = new TextWidget;
    topic_QoS_value_ = new TextWidget(QString::number(topic_.subscription->qos()));
    topic_current_value_ = new TextWidget(QString());

    connect(topic_.subscription, &QMqttSubscription::stateChanged, this, &SubscriptionWidget::updateStatus);
    connect(topic_.subscription, &QMqttSubscription::qosChanged,
        [&, this](quint8 qos) {
            topic_QoS_value_->setText(QString::number(qos));
        });
    connect(subscription_btn_, &AnimatedButton::clicked, [this]() {
        emit topicDeleteRequested(topic_);
    });
    connect(topic_.subscription, &QMqttSubscription::stateChanged,
        this, &SubscriptionWidget::updateStatus);
    connect(topic_.subscription, &QMqttSubscription::messageReceived,
        [this](const QMqttMessage& msg) {
            topic_current_value_->setText(msg.payload());
        });

    content_area = new BaseWidget();
    content_area->setColorScheme(ColorScheme::PRIMARY);
    content_area->setRadius(8);
    auto content_layout = new QGridLayout;
    content_area->setLayout(content_layout);
    QSizePolicy sp_retain = content_area->sizePolicy();
    sp_retain.setRetainSizeWhenHidden(true);
    content_area->setSizePolicy(sp_retain);
    content_area->setFixedHeight(0);

    content_layout->addWidget(new TextWidget(tr("Topic name"), 10, true), 0, 0);
    content_layout->addWidget(topic_label_value_, 0, 1, 1, 5);
    content_layout->addWidget(new TextWidget(tr("Connection status"), 10, true), 1, 0);
    content_layout->addWidget(topic_status_value_, 1, 1, 1, 2);
    content_layout->addWidget(new TextWidget(tr("Quality of service"), 10, true), 1, 3);
    content_layout->addWidget(topic_QoS_value_, 1, 4, 1, 2);
    content_layout->addWidget(new TextWidget(tr("Current value"), 10, true), 2, 0);
    content_layout->addWidget(topic_current_value_, 2, 1, 1, 4);

    main_layout->addWidget(header);
    main_layout->addWidget(content_area);

    animation = new QPropertyAnimation(this, "hidden_height");
    animation->setDuration(200);
    connect(animation,
        &QPropertyAnimation::finished,
        this,
        [=]() {
            if (content_area->height())
                content_area->show();
        });
    updateStatus(topic_.subscription->state());
}

void SubscriptionWidget::updateStatus(QMqttSubscription::SubscriptionState state)
{
    switch (state) {
    case QMqttSubscription::Unsubscribed:
        topic_status_value_->setText(QLatin1String("Unsubscribed"));
        break;
    case QMqttSubscription::SubscriptionPending:
        topic_status_value_->setText(QLatin1String("Pending"));
        break;
    case QMqttSubscription::Subscribed:
        topic_status_value_->setText(QLatin1String("Subscribed"));
        break;
    case QMqttSubscription::Error:
        topic_status_value_->setText(QLatin1String("Error"));
        break;
    default:
        topic_status_value_->setText(QLatin1String("--Unknown--"));
        break;
    }
}

int SubscriptionWidget::getHeight()
{
    return content_area->height();
}

void SubscriptionWidget::setHeight(int value)
{
    content_area->setFixedHeight(value);
    update();
}
