#include "mainwindowwrapper.h"

MainWindowWrapper::MainWindowWrapper(QWidget* parent)
    : QMainWindow(parent)
{
    setCentralWidget(new MainWindow);
}
