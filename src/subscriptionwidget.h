#ifndef SUBSCRIPTIONWIDGET_H
#define SUBSCRIPTIONWIDGET_H

#include <QLabel>
#include <QtMqtt/QMqttSubscription>
#include <base/animatedbutton.h>
#include <base/animatedwidget.h>
#include <base/textwidget.h>
#include <topicmodel.h>

class SubscriptionWidget : public BaseWidget {
    Q_OBJECT
    Q_PROPERTY(int hidden_height READ getHeight WRITE setHeight)
public:
    SubscriptionWidget(TopicStruct& topic, QWidget* parent = nullptr);

private slots:
    void updateStatus(QMqttSubscription::SubscriptionState state);
signals:
    void topicDeleteRequested(const TopicStruct& topic);

private:
    TopicStruct topic_;
    TextWidget
        *topic_label_value_,
        *topic_alias_value_,
        *topic_QoS_value_,
        *topic_status_value_,
        *topic_current_value_;
    AnimatedButton* subscription_btn_;

private:
    BaseWidget* content_area;
    QPropertyAnimation* animation; ///< Expand/collapse hidden_frame animation object
    int getHeight();
    void setHeight(int value);
};

#endif // SUBSCRIPTIONWIDGET_H
