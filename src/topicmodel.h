#ifndef TOPICMODEL_H
#define TOPICMODEL_H

#include <QString>
#include <QVariant>
#include <QtMqtt/QMqttClient>
#include <QtMqtt/QMqttSubscription>

struct TopicStruct {
    QString alias;
    QVariant value;
    QMetaType value_type;
    QMqttSubscription* subscription;
};

class TopicModel {
public:
    TopicModel();
};

#endif // TOPICMODEL_H
