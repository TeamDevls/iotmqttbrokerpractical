#ifndef BOILERPLATE_H
#define BOILERPLATE_H
#include <QLineEdit>
#include <base/appstyle.h>

namespace BoilerPlate {
QLineEdit* createLineEdit(int min_h, ColorScheme scheme);
void changeBottomBorder(QLineEdit* target);
}

#endif // BOILERPLATE_H
