#ifndef MAINWINDOWWRAPPER_H
#define MAINWINDOWWRAPPER_H

#include <QMainWindow>
#include <mainwindow.h>

class MainWindowWrapper : public QMainWindow {
    Q_OBJECT
public:
    explicit MainWindowWrapper(QWidget* parent = nullptr);
};

#endif // MAINWINDOWWRAPPER_H
