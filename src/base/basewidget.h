#ifndef BASEWIDGET_H
#define BASEWIDGET_H

#include <QPainter>
#include <QWidget>
#include <base/appstyle.h>

class BaseWidget : public QWidget {
    Q_OBJECT
public:
    explicit BaseWidget(QWidget* parent = nullptr);
    void setColorScheme(ColorScheme color_scheme)
    {
        color_scheme_ = color_scheme;
        update();
    }
    ColorScheme colorScheme() { return color_scheme_; }
    void setRadius(int r)
    {
        r_ = r;
        update();
    }
    int radius() { return r_; }
    void setState(STATE state)
    {
        state_ = state;
        update();
    }
    STATE state() { return state_; }

protected:
    virtual void paintEvent(QPaintEvent*) override;
    int r_;
    ColorScheme color_scheme_;
    STATE state_;
};

#endif // BASEWIDGET_H
