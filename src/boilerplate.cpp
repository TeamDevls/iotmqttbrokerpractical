#include <boilerplate.h>

QLineEdit* BoilerPlate::createLineEdit(int min_h, ColorScheme scheme)
{
    auto e = new QLineEdit;
    e->setAlignment(Qt::AlignCenter);
    e->setMinimumHeight(min_h);
    e->setStyleSheet(QString("QLineEdit {background: white; border: 0px; border-bottom: 2px solid %1; }")
                         .arg(AppStyle::backgroundColor(scheme, STATE::HOVERED).name()));

    return e;
}

void BoilerPlate::changeBottomBorder(QLineEdit* target)
{
    QString color;
    if (target->text() == "")
        color = AppStyle::backgroundColor(ColorScheme::SECONDARY, STATE::HOVERED).name();
    else if (target->hasAcceptableInput())
        color = "rgb(51,138,62)";
    else
        color = "rgb(195,0,0)";
    target->setStyleSheet(QString("QLineEdit {background: white; border: 0px; border-bottom: 2px solid %1; }")
                              .arg(color));
}
