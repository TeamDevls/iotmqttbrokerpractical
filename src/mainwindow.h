#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QApplication>
#include <QBuffer>
#include <QCheckBox>
#include <QFileDialog>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPlainTextEdit>
#include <QSpinBox>
#include <QTimer>
#include <QWidget>
#include <QXmlStreamWriter>
#include <QtMqtt/QMqttClient>
#include <base/animatedbutton.h>
#include <boilerplate.h>
#include <dialogs/newsubscription.h>
#include <dialogs/publishdialog.h>
#include <topicmodel.h>

class MainWindow : public BaseWidget {
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();
public slots:
    void setClientPort(int p);

private slots:
    void on_buttonConnect_clicked();
    void on_buttonQuit_clicked();
    void brokerDisconnected();
    void connectionParamsChanged();
    void onWriteTimerTimeout();

private:
    QLineEdit
        *lineEditHost,
        *lineEditUser,
        *lineEditPassword,
        *lineEditMessage,
        *line_client_port;
    QSpinBox* spinQoS_2;
    AnimatedButton
        *buttonConnect,
        *buttonPing,
        *buttonPublish,
        *add_new_topic_btn,
        *write_data_files_btn,
        *publish_msg_btn;
    QCheckBox* checkBoxRetain;
    ScrollAreaWrapper* subscriptions_list_;
    QMqttClient* client_;
    QHash<QString, TopicStruct> data_;
    QJsonArray json_temp_;
    QXmlStreamWriter xml_writer;
    QBuffer* xml_temp_;
    QTimer* write_timer_;

    void startWritingFiles();
    void stopWritingFiles();
    void writeCurrentIntoJson();
    void writeCurrentIntoXml();
};
#endif // MAINWINDOW_H
