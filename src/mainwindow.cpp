#include "mainwindow.h"

#include <QGroupBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget* parent)
    : BaseWidget(parent)
{
    setMinimumSize(600, 400);
    setColorScheme(ColorScheme::SECONDARY);
    auto layout = new QVBoxLayout;
    setLayout(layout);

    auto group_box_1 = new QGroupBox("Connection");
    buttonConnect = new AnimatedButton("Connect", ColorScheme::SECONDARY, 8);
    auto sp = buttonConnect->sizePolicy();
    sp.setRetainSizeWhenHidden(true);
    buttonConnect->setSizePolicy(sp);
    buttonConnect->hide();
    buttonPing = new AnimatedButton("Ping", ColorScheme::SECONDARY, 8);
    buttonPing->setSizePolicy(sp);
    buttonPing->hide();
    auto btn_heigth = buttonConnect->sizeHint().height() * 1.5;
    lineEditHost = BoilerPlate::createLineEdit(btn_heigth, ColorScheme::SECONDARY);
    lineEditUser = BoilerPlate::createLineEdit(btn_heigth, ColorScheme::SECONDARY);
    lineEditPassword = BoilerPlate::createLineEdit(btn_heigth, ColorScheme::SECONDARY);
    line_client_port = BoilerPlate::createLineEdit(btn_heigth, ColorScheme::SECONDARY);
    auto port_validator = new QIntValidator(0, 65536, this);
    auto host_validator = new QRegularExpressionValidator(
        QRegularExpression(
            "^(http\\:\\/\\/|https\\:\\/\\/)?([a-z0-9][a-z0-9\\-]*\\.)+[a-z0-9][a-z0-9\\-]*$"),
        this);
    lineEditHost->setValidator(host_validator);
    line_client_port->setValidator(port_validator);
    buttonConnect->setMinimumHeight(btn_heigth);
    buttonPing->setMinimumHeight(btn_heigth);
    auto group_layout_1 = new QGridLayout;
    group_box_1->setLayout(group_layout_1);
    group_layout_1->addWidget(new QLabel("Host: "), 0, 0, 1, 1);
    group_layout_1->addWidget(lineEditHost, 0, 1, 1, 8);
    group_layout_1->addWidget(buttonConnect, 0, 9);
    group_layout_1->addWidget(new QLabel("Port: "), 1, 0, 1, 1);
    group_layout_1->addWidget(line_client_port, 1, 1, 1, 8);
    group_layout_1->addWidget(buttonPing, 1, 9);
    group_layout_1->addWidget(new QLabel("User: "), 2, 0, 1, 1);
    group_layout_1->addWidget(lineEditUser, 2, 1, 1, 8);
    group_layout_1->addWidget(new QLabel("Password: "), 3, 0, 1, 1);
    group_layout_1->addWidget(lineEditPassword, 3, 1, 1, 8);
    layout->addWidget(group_box_1);
    /*
    auto group_box_3 = new QGroupBox("Messages");
    auto group_layout_3 = new QGridLayout;
    group_box_3->setLayout(group_layout_3);
    lineEditMessage = BoilerPlate::createLineEdit(btn_heigth, ColorScheme::SECONDARY);
    spinQoS_2 = new QSpinBox;
    spinQoS_2->setRange(0, 2);
    checkBoxRetain = new QCheckBox;
    buttonPublish = new AnimatedButton("Publish", ColorScheme::SECONDARY, 8);
    group_layout_3->addWidget(new QLabel("Content: "), 0, 0, 1, 1);
    group_layout_3->addWidget(lineEditMessage, 0, 1, 1, 8);
    group_layout_3->addWidget(buttonPublish, 0, 9);
    group_layout_3->addWidget(new QLabel("QoS: "), 1, 0, 1, 1);
    group_layout_3->addWidget(spinQoS_2, 1, 1, 1, 8);
    group_layout_3->addWidget(new QLabel("Retain: "), 2, 0, 1, 1);
    group_layout_3->addWidget(checkBoxRetain, 2, 1);

    layout->addWidget(group_box_3);
*/
    auto group_box_4 = new QGroupBox("Subscriptions");
    add_new_topic_btn = new AnimatedButton("Add new subscription", ColorScheme::SECONDARY, 8);
    add_new_topic_btn->setSizePolicy(sp);
    add_new_topic_btn->hide();
    add_new_topic_btn->setMinimumHeight(add_new_topic_btn->sizeHint().height() * 1.5);
    publish_msg_btn = new AnimatedButton("Publish new message", ColorScheme::SECONDARY, 8);
    publish_msg_btn->setSizePolicy(sp);
    publish_msg_btn->hide();
    publish_msg_btn->setMinimumHeight(publish_msg_btn->sizeHint().height() * 1.5);
    write_data_files_btn = new AnimatedButton("Start writing files", ColorScheme::SECONDARY, 8);
    write_data_files_btn->setSizePolicy(sp);
    write_data_files_btn->hide();
    write_data_files_btn->setMinimumHeight(write_data_files_btn->sizeHint().height() * 1.5);
    auto group_layout_4 = new QVBoxLayout;
    group_box_4->setLayout(group_layout_4);
    subscriptions_list_ = new ScrollAreaWrapper(this);
    subscriptions_list_->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    subscriptions_list_->scroll_layout->addStretch();
    group_layout_4->addWidget(subscriptions_list_);
    group_layout_4->addWidget(add_new_topic_btn);
    group_layout_4->addWidget(publish_msg_btn);
    group_layout_4->addWidget(write_data_files_btn);
    layout->addWidget(group_box_4);

    client_ = new QMqttClient(this);
    write_timer_ = nullptr;

    connect(add_new_topic_btn, &AnimatedButton::clicked, [this]() {
        auto d = new NewSubscription(client_, data_, subscriptions_list_, parentWidget(), this);
        d->open();
    });
    connect(publish_msg_btn, &AnimatedButton::clicked, [this]() {
        auto d = new PublishDialog(client_, data_, parentWidget(), this);
        d->open();
    });
    connect(write_data_files_btn, &AnimatedButton::clicked,
        [this]() {
            if (write_timer_)
                stopWritingFiles();
            else
                startWritingFiles();
        });
    connect(buttonConnect, &AnimatedButton::clicked, this, &MainWindow::on_buttonConnect_clicked);
    connect(buttonPing, &AnimatedButton::clicked, [this]() {
        client_->requestPing();
    });

    connect(client_, &QMqttClient::disconnected, this, &MainWindow::brokerDisconnected);

    connect(line_client_port, &QLineEdit::textChanged,
        [this](const QString& text) {
            BoilerPlate::changeBottomBorder(line_client_port);
            if (line_client_port->hasAcceptableInput()) {
                setClientPort(text.toInt());
                connectionParamsChanged();
            }
        });
    connect(lineEditHost, &QLineEdit::textChanged,
        [this](const QString& text) {
            BoilerPlate::changeBottomBorder(lineEditHost);
            if (lineEditHost->hasAcceptableInput()) {
                client_->setHostname(text);
                connectionParamsChanged();
            }
        });
    connect(lineEditUser, &QLineEdit::textChanged,
        [this](const QString& text) {
            BoilerPlate::changeBottomBorder(lineEditUser);
            if (lineEditUser->hasAcceptableInput())
                client_->setUsername(text);
        });
    connect(lineEditPassword, &QLineEdit::textChanged,
        [this](const QString& text) {
            BoilerPlate::changeBottomBorder(lineEditPassword);
            if (lineEditPassword->hasAcceptableInput())
                client_->setPassword(text);
        });

    connect(client_, &QMqttClient::messageReceived, this,
        [this](const QByteArray& msg, const QMqttTopicName& topic) {
            data_[topic.name()].value = msg;
            data_[topic.name()].value.convert(data_[topic.name()].value_type);
        });
    dumpObjectInfo();
    dumpObjectTree();
    client_->dumpObjectInfo();
}
MainWindow::~MainWindow()
{
}
void MainWindow::on_buttonConnect_clicked()
{
    if (client_->state() == QMqttClient::Disconnected) {
        lineEditHost->setReadOnly(true);
        line_client_port->setReadOnly(true);
        lineEditUser->setReadOnly(true);
        lineEditPassword->setReadOnly(true);
        buttonConnect->setText(tr("Disconnect"));
        client_->connectToHost();
        buttonPing->show();
        add_new_topic_btn->show();
        publish_msg_btn->show();
        write_data_files_btn->show();
    } else {
        lineEditHost->setReadOnly(false);
        line_client_port->setReadOnly(false);
        lineEditUser->setReadOnly(false);
        lineEditPassword->setReadOnly(false);
        buttonConnect->setText(tr("Connect"));
        client_->disconnectFromHost();
    }
}

void MainWindow::on_buttonQuit_clicked()
{
    QApplication::quit();
}

void MainWindow::brokerDisconnected()
{
    buttonPing->hide();
    add_new_topic_btn->hide();
    publish_msg_btn->hide();
    write_data_files_btn->hide();
    lineEditHost->setReadOnly(false);
    line_client_port->setReadOnly(false);
    lineEditUser->setReadOnly(false);
    lineEditPassword->setReadOnly(false);
    buttonConnect->setText(tr("Connect"));
}

void MainWindow::connectionParamsChanged()
{
    if (lineEditHost->hasAcceptableInput()
        && line_client_port->hasAcceptableInput()
        && lineEditUser->hasAcceptableInput()
        && lineEditPassword->hasAcceptableInput()) {
        buttonConnect->show();
    } else {
        buttonConnect->hide();
    }
}

void MainWindow::startWritingFiles()
{
    xml_temp_ = new QBuffer(this);
    xml_writer.setAutoFormatting(true);
    xml_writer.setDevice(xml_temp_);
    xml_temp_->open(QIODevice::WriteOnly);
    xml_writer.writeStartDocument();
    xml_writer.writeStartElement("data");

    write_timer_ = new QTimer(this);
    connect(write_timer_, &QTimer::timeout,
        this, &MainWindow::onWriteTimerTimeout);
    write_timer_->start(5000);
    write_data_files_btn->setText(tr("Stop writing files and save"));
}

void MainWindow::stopWritingFiles()
{
    write_timer_->stop();
    write_timer_->deleteLater();
    write_timer_ = nullptr;
    write_data_files_btn->setText(tr("Start writing files"));

    xml_writer.writeEndElement();
    xml_writer.writeEndDocument();
    QString xml_filename = QFileDialog::getSaveFileName(
        this, tr("Save Xml"), ".", tr("Xml files (*.xml)"));
    if (xml_filename != QStringLiteral("")) {
        QFile xml_file(xml_filename);
        xml_file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);
        xml_file.write(xml_temp_->data());
        xml_temp_->close();
        xml_temp_->deleteLater();
        xml_file.close();
    }

    QString json_filename = QFileDialog::getSaveFileName(
        this, tr("Save Json"), ".", tr("Json files (*.json)"));
    if (json_filename != QStringLiteral("")) {
        QFile json_file(json_filename);
        QJsonObject data;
        data.insert("data", json_temp_);
        QJsonDocument doc(data);
        json_file.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
        json_file.write(doc.toJson());
        json_file.close();
        json_temp_ = QJsonArray();
    }
}

void MainWindow::onWriteTimerTimeout()
{
    if (data_.size()) {
        writeCurrentIntoJson();
        writeCurrentIntoXml();
    }
}

void MainWindow::writeCurrentIntoJson()
{
    QJsonObject current;
    for (auto& each : data_)
        current.insert(each.alias, each.value.toJsonValue());
    auto host = client_->hostname();
    if (auto last_dot_ix = host.lastIndexOf("."); last_dot_ix != -1) {
        auto last_ip_byte = host.mid(last_dot_ix + 1).toInt();
        current.insert("ID", last_ip_byte);
    }
    current.insert("timestamp", QDateTime::currentDateTime().toString("yyyy-mm-ddThh:mm:ss.zzz"));
    json_temp_.push_back(current);
}

void MainWindow::writeCurrentIntoXml()
{
    xml_writer.writeStartElement("item");
    for (auto& each : data_) {
        auto alias = each.alias.replace(QChar('/'), QChar('_'));
        xml_writer.writeStartElement(alias);
        xml_writer.writeCharacters(each.value.toString());
        xml_writer.writeEndElement();
    }
    auto host = client_->hostname();
    if (auto last_dot_ix = host.lastIndexOf("."); last_dot_ix != -1) {
        auto last_ip_byte = host.mid(last_dot_ix + 1);
        xml_writer.writeStartElement("ID");
        xml_writer.writeCharacters(last_ip_byte);
        xml_writer.writeEndElement();
    }
    xml_writer.writeStartElement("timestamp");
    xml_writer.writeCharacters(QDateTime::currentDateTime().toString("yyyy-mm-ddThh:mm:ss.zzz"));
    xml_writer.writeEndElement();
    xml_writer.writeEndElement();
}

void MainWindow::setClientPort(int p)
{
    client_->setPort(p);
}
