#ifndef NEWSUBSCRIPTION_H
#define NEWSUBSCRIPTION_H

#include "roundeddialog.h"
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <base/scrollareawrapper.h>
#include <boilerplate.h>
#include <subscriptionwidget.h>
#include <topicmodel.h>

class NewSubscription : public RoundedDialog {
public:
    NewSubscription(QMqttClient* client,
        QHash<QString, TopicStruct>& topics,
        ScrollAreaWrapper* subscription_list,
        QWidget* parent = nullptr,
        QWidget* dimmable_widget = nullptr);
};

#endif // NEWSUBSCRIPTION_H
