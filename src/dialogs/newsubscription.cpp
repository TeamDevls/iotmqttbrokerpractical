#include "newsubscription.h"

#include <QComboBox>
#include <QIntValidator>
#include <QMessageBox>

NewSubscription::NewSubscription(
    QMqttClient* client,
    QHash<QString, TopicStruct>& topics,
    ScrollAreaWrapper* subscription_list,
    QWidget* parent,
    QWidget* dimmable_widget)
    : RoundedDialog(parent, dimmable_widget)
{
    setStyleSheet("QComboBox {background: white; border: 0px; padding: 4px}"
                  "QComboBox::down-arrow { border: 0px; width: 0px;}"
                  "QComboBox::drop-down { border: 0px}");
    setMinimumWidth(400);
    setMinimumHeight(250);
    auto layout = new QGridLayout;
    setLayout(layout);

    title_->setText("New Subscription");

    auto topic = new QLineEdit;
    BoilerPlate::changeBottomBorder(topic);
    auto topic_alias = new QLineEdit;
    BoilerPlate::changeBottomBorder(topic_alias);
    auto topic_QoS = new QComboBox();

    topic_QoS->addItem("0");
    topic_QoS->addItem("1");
    topic_QoS->addItem("2");
    auto value_type = new QComboBox();
    value_type->addItem("string", QString());
    value_type->addItem("int", int());
    value_type->addItem("bool", bool());
    value_type->addItem("double", double());

    auto controls_layout = new QHBoxLayout;
    controls_layout->setSpacing(8);
    auto ok_btn = new AnimatedButton("Subscribe",
        ColorScheme::SECONDARY,
        AppStyle::mediumCornerRadius());
    ok_btn->setFontParams(10, true);
    connect(ok_btn,
        &AnimatedButton::clicked,
        [&topics, topic, topic_QoS, topic_alias, client, subscription_list, value_type, this]() {
            auto topic_text = topic->text();
            auto topic_alias_text = (topic_alias->text() == "") ? topic_text
                                                                : topic_alias->text();
            auto subscription = client->subscribe(topic_text, topic_QoS->currentIndex());
            if (!subscription) {
                QMessageBox::critical(this, QLatin1String("Error"), QLatin1String("Could not subscribe. Is there a valid connection?"));
                return;
            }
            topics[topic_text].subscription = subscription;
            topics[topic_text].alias = topic_alias_text;
            topics[topic_text].value_type = value_type->currentData().metaType();
            topics[topic_text].value = QString();

            auto subscription_widget = new SubscriptionWidget(topics[topic_text]);
            connect(
                subscription_widget,
                &SubscriptionWidget::topicDeleteRequested,
                [&topics, subscription_widget](const TopicStruct& topic) {
                    topics.remove(topic.subscription->topic().filter());
                    topic.subscription->unsubscribe();
                    subscription_widget->deleteLater();
                });
            auto list_layout = subscription_list->scroll_layout;
            list_layout->insertWidget(list_layout->count() - 1, subscription_widget);
            closeDialog();
        });

    ok_btn->setMinimumHeight(ok_btn->sizeHint().height() * 2);
    auto cancel_btn = new AnimatedButton("Cancel",
        ColorScheme::SECONDARY,
        AppStyle::mediumCornerRadius());
    cancel_btn->setFontParams(10, true);
    cancel_btn->setMinimumHeight(cancel_btn->sizeHint().height() * 2);
    connect(cancel_btn, &AnimatedButton::clicked,
        this, &NewSubscription::closeDialog);
    controls_layout->addWidget(ok_btn);
    controls_layout->addWidget(cancel_btn);

    layout->addWidget(title_, 0, 0, 2, 5);
    layout->addWidget(new TextWidget("Topic", 10, true, ColorScheme::TRANSPARENT), 2, 0, 2, 1);
    layout->addWidget(topic, 2, 1, 2, 4);
    layout->addWidget(new TextWidget("Topic alias", 10, true, ColorScheme::TRANSPARENT), 4, 0, 2, 1);
    layout->addWidget(topic_alias, 4, 1, 2, 4);
    layout->addWidget(new TextWidget("Qos", 10, true, ColorScheme::TRANSPARENT), 6, 2, 2, 1);
    layout->addWidget(topic_QoS, 6, 3, 2, 1);
    layout->addWidget(new TextWidget("Value type", 10, true, ColorScheme::TRANSPARENT), 6, 0, 2, 1);
    layout->addWidget(value_type, 6, 1, 2, 1);
    layout->addLayout(controls_layout, 10, 0, 1, 5);
}
