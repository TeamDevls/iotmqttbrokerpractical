#include "publishdialog.h"

PublishDialog::PublishDialog(QMqttClient* client,
    QHash<QString, TopicStruct>& topics,
    QWidget* parent,
    QWidget* dimmable_widget)
    : RoundedDialog(parent, dimmable_widget)
{
    setStyleSheet("QComboBox {background: white; border: 0px; padding: 4px}"
                  "QComboBox::down-arrow { border: 0px; width: 0px;}"
                  "QComboBox::drop-down { border: 0px}");
    setMinimumWidth(400);
    setMinimumHeight(270);
    auto layout = new QGridLayout;
    setLayout(layout);

    title_->setText("Publish message");

    auto topic_alias = new QComboBox();
    QHashIterator<QString, TopicStruct> it(topics);
    while (it.hasNext()) {
        it.next();
        topic_alias->addItem(it.value().alias, it.key());
    }

    auto topic_QoS = new QComboBox();
    topic_QoS->addItem("0");
    topic_QoS->addItem("1");
    topic_QoS->addItem("2");

    auto topic_message = new QLineEdit;
    BoilerPlate::changeBottomBorder(topic_message);

    auto topic_retain = new QComboBox();
    topic_retain->addItem("No");
    topic_retain->addItem("Yes");

    auto controls_layout = new QHBoxLayout;
    controls_layout->setSpacing(8);
    auto ok_btn = new AnimatedButton("Publish",
        ColorScheme::SECONDARY,
        AppStyle::mediumCornerRadius());
    ok_btn->setFontParams(10, true);
    connect(ok_btn,
        &AnimatedButton::clicked,
        [topic_alias, topic_QoS, topic_message, topic_retain, client, this]() {
            if (client->publish(topic_alias->currentData().toString(),
                    topic_message->text().toUtf8(),
                    topic_QoS->currentIndex(),
                    topic_retain->currentIndex())
                == -1)
                QMessageBox::critical(this, QLatin1String("Error"), QLatin1String("Could not publish message"));
        });

    ok_btn->setMinimumHeight(ok_btn->sizeHint().height() * 2);
    auto cancel_btn = new AnimatedButton("Cancel",
        ColorScheme::SECONDARY,
        AppStyle::mediumCornerRadius());
    cancel_btn->setFontParams(10, true);
    cancel_btn->setMinimumHeight(cancel_btn->sizeHint().height() * 2);
    connect(cancel_btn, &AnimatedButton::clicked,
        this, &PublishDialog::closeDialog);
    controls_layout->addWidget(ok_btn);
    controls_layout->addWidget(cancel_btn);

    layout->addWidget(title_, 0, 0, 2, 5);
    layout->addWidget(new TextWidget("Topic", 10, true, ColorScheme::TRANSPARENT), 4, 0, 2, 1);
    layout->addWidget(topic_alias, 4, 1, 2, 4);
    layout->addWidget(new TextWidget("Message", 10, true, ColorScheme::TRANSPARENT), 6, 0, 2, 1);
    layout->addWidget(topic_message, 6, 1, 2, 4);
    layout->addWidget(new TextWidget("Qos", 10, true, ColorScheme::TRANSPARENT), 8, 0, 2, 1);
    layout->addWidget(topic_QoS, 8, 1, 2, 1);
    layout->addWidget(new TextWidget("Retain", 10, true, ColorScheme::TRANSPARENT), 8, 2, 2, 1);
    layout->addWidget(topic_retain, 8, 3, 2, 1);
    layout->addLayout(controls_layout, 10, 0, 1, 5);
}
