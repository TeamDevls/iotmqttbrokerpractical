#ifndef PUBLISHDIALOG_H
#define PUBLISHDIALOG_H

#include "roundeddialog.h"
#include <QComboBox>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QtMqtt/QMqttClient>
#include <base/textwidget.h>
#include <boilerplate.h>
#include <topicmodel.h>

class PublishDialog : public RoundedDialog {
public:
    PublishDialog(QMqttClient* client,
        QHash<QString, TopicStruct>& topics,
        QWidget* parent = nullptr,
        QWidget* dimmable_widget = nullptr);
};

#endif // PUBLISHDIALOG_H
