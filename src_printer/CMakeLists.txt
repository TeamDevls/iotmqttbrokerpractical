
set(CMAKE_INCLUDE_CURRENT_DIR ON)

#find_package(QT NAMES Qt6 Qt5 COMPONENTS Gui Widgets Core REQUIRED)
find_package(Qt6 COMPONENTS REQUIRED Gui Widgets Core Mqtt )

file(GLOB_RECURSE HEADERS *.h)
file(GLOB_RECURSE SOURCES *.cpp)
file(GLOB_RECURSE QRC *.qrc)
file(GLOB_RECURSE UI *.ui)

add_executable(${CMAKE_PROJECT_NAME}_printer ${SOURCES} ${HEADERS} ${QRC} ${UI})
target_link_libraries(${CMAKE_PROJECT_NAME}_printer PRIVATE Qt6::Widgets Qt6::Gui Qt6::Core Qt6::Mqtt)
