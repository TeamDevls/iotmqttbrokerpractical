#include <QApplication>

#include <QFile>
#include <QFileDialog>
#include <QJsonDocument>
#include <QString>
#include <QXmlStreamReader>

void printXml(const QString& path)
{
    QFile xml(path);
    xml.open(QIODevice::ReadOnly);
    QXmlStreamReader reader(&xml);
    reader.readNext();
    while (!reader.atEnd() && !reader.hasError()) {
        reader.readNext();
        if (reader.isStartDocument() || reader.isEndDocument())
            continue;
        auto name = reader.name().toString();
        if (name == QStringLiteral("data"))
            qDebug() << (reader.isStartElement() ? "<data>"
                                                 : "</data>");
        else if (name == QStringLiteral("item"))
            qDebug() << (reader.isStartElement() ? "    <item>"
                                                 : "    </item>");
        else if (reader.isStartElement()) {
            qDebug().noquote() << QString("    <%1>%2</%1>").arg(name, reader.readElementText());
        }
    }
}

void printJson(const QString& path)
{
    QFile json(path);
    json.open(QIODevice::ReadOnly);
    QJsonDocument json_doc = QJsonDocument::fromJson(json.readAll());
    json.close();

    auto data_obj = json_doc.toVariant().toMap();
    qDebug().noquote() << data_obj.begin().key() << ":";
    for (auto& each_item : data_obj.begin().value().toList()) {
        qDebug() << "{";
        auto each = each_item.toMap();
        QMapIterator<QString, QVariant> it(each);
        while (it.hasNext()) {
            it.next();
            qDebug().noquote() << "    " << it.key() << ":" << it.value();
        }
        qDebug() << "}";
    }
}

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    QTextStream out(stdout);

    QString xml_filename = QFileDialog::getOpenFileName(
        nullptr, a.tr("Open xml"), ".", a.tr("Xml files (*.xml)"));
    if (xml_filename != QStringLiteral(""))
        printXml(xml_filename);

    QString json_filename = QFileDialog::getOpenFileName(
        nullptr, a.tr("Open json"), ".", a.tr("Json files (*.json)"));
    if (json_filename != QStringLiteral(""))
        printJson(json_filename);

    a.quit();
    return a.exec();
}
