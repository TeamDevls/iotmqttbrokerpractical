#include <QApplication>
#include <QMqttClient>
#include <QRandomGenerator>
#include <QString>
#include <QTimer>

auto generateBool()
{
    return QString::number(QRandomGenerator::global()->bounded(0, 2));
}

auto generateInt()
{
    return QString::number(QRandomGenerator::global()->generate());
}

auto generateDouble()
{
    auto d = static_cast<double>(QRandomGenerator::global()->bounded(1, 10000))
        / QRandomGenerator::global()->bounded(1, 100);
    return QString::number(d);
}

auto generateString()
{
    const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
    const int randomStringLength = 12; // assuming you want random strings of 12 characters

    QString randomString;
    for (int i = 0; i < randomStringLength; ++i) {
        int index = QRandomGenerator::global()->generate() % possibleCharacters.length();
        QChar nextChar = possibleCharacters.at(index);
        randomString.append(nextChar);
    }
    return randomString;
}

int main(int argc, char* argv[])
{
    if (argc < 3)
        return 1;

    QApplication a(argc, argv);

    auto client = new QMqttClient(&a);
    client->setHostname(argv[1]);
    client->setPort(QString(argv[2]).toUInt());
    if (argc == 3)
        client->setUsername(argv[3]);
    if (argc == 4)
        client->setPassword(argv[4]);
    client->connectToHost();

    auto timer = new QTimer(&a);
    a.connect(timer, &QTimer::timeout, [client]() {
        client->publish(QStringLiteral("test/bool"), generateBool().toUtf8(), 2, false);
        client->publish(QStringLiteral("test/double"), generateDouble().toUtf8(), 2, false);
        client->publish(QStringLiteral("test/int"), generateInt().toUtf8(), 2, false);
        client->publish(QStringLiteral("test/string"), generateString().toUtf8(), 2, false);
    });
    timer->start(5000);

    return a.exec();
}
